import React from 'react'
import { phongTrodata } from './data/data'
import ItemPhong from './ItemPhong'
import FormSearch from './FormSearch'

export default function ListPhong() {
let renderPhong = ()=>{
 return   phongTrodata.map((item)=>{
      return <ItemPhong item={item}/>
    })
  }
  return (
   <div>
    <FormSearch renderPhong={renderPhong}/>
    <div className='row'>
   {renderPhong()}

    </div>
   </div>
  )
}

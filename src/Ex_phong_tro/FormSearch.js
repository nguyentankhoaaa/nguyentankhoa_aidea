import React, { useState } from 'react'
import { phongTrodata } from './data/data';
import { quanhuyenData } from './data/quan_huyen';
import { tinhTpData } from './data/tinh_tp';
import ListPhong from './ListPhong'
import ItemPhong from './ItemPhong';

export default function FormSearch() {
    const [selectedProvince, setSelectedProvince] = useState('');
    const [selectedDistrict, setSelectedDistrict] = useState('');
    const [selectedPrice, setSelectedPrice] = useState('');
    const arrTinhTp = Object.entries(tinhTpData);
const arrQuanhuyen = Object.entries(quanhuyenData);
const handleProvinceChange = (e) => {
    setSelectedProvince(e.target.value);
  };

  const handleDistrictChange = (e) => {
    setSelectedDistrict(e.target.value);
  };
  const handlePriceChange = (e) => {
  setSelectedPrice(e.target.value);
  };
  let filterQuanHuyen = ()=>{
   return arrQuanhuyen.filter(item=>item[1].parent_code==selectedProvince)
  }
  let HandlefilterListPhongTro=()=>{
return phongTrodata.map((item)=>{
  if(item.city===selectedProvince && item.district===selectedDistrict && item.price===selectedPrice){
     return <ItemPhong item={item}/>
  }else{
    return <h3>Không tìm thấy phòng</h3>
    }
})
  }

  return (

  <div>
      <div className='FormSearch' 
        style={{padding:"15px 20px",backgroundColor:"#FFE4B5",borderRadius:"10px"}}>
<form action="" style={{display:"inline-flex",gap:"30px"}}>
<div className='tinhThanhSelect'>
    <h6 >Tỉnh Thành</h6>
<select name="Chọn Tỉnh Thành" id="province" value={selectedProvince} onChange={handleProvinceChange}>
    <option value="" >Chọn Tỉnh Thành</option>
    {arrTinhTp.map((item)=>{
        return <option value={item[1].code}>{item[1].name}</option>
    })}
</select>

</div>

<div className='QuanHuyenSelect'>
    <h6>Quận Huyện</h6>
<select name="Chọn Tỉnh Thành" value={selectedDistrict} onChange={handleDistrictChange} id="">
    <option value="" >Chọn Quận Huyện</option>
    {filterQuanHuyen().map((item)=>{
        return <option value={item[1].code}>{item[1].name}</option>
    })}
</select>

</div>
<div className='KhoangGiaSelect'>
    <h6>Khoảng Giá</h6>
<select name="Chọn Tỉnh Thành" id="" value={selectedPrice} onChange={handlePriceChange}>
    <option value="" >Chọn Khoảng giá</option>
    {phongTrodata.map((item)=>{
        return <option value={item.price} >{item.price}</option>
    })}
</select>

</div>

<div>
<button className='btn btn-warning text-white'style={{marginTop:"25px",padding:"2px 10px"}} onClick={()=>{
  HandlefilterListPhongTro()
}}>Lọc Tìm</button>
</div>



</form>
    </div>

  </div>
  )
}

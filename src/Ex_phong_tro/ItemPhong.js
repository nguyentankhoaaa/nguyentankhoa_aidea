import React from 'react'

export default function ItemPhong({item}) {
  return (
    <div className='col-4 ' style={{marginTop:"40px"}}>
<div className="card" style={{width: '20rem'}}>
  <img className="card-img-top"style={{width: '20rem',height:"15em"}} src={item.thumbnail} alt="Card image cap" />
  <div className="card-body">
    <h5 className="card-title">{item.title}</h5>
    <p className="card-text">{item.content}</p>
   <h2>{item.price}đ</h2>
  </div>
</div>

    </div>
  )
}
